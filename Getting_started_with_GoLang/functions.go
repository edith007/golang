package main

import (
	"fmt"
)

func main() {
	sayMessage("Hello, GoLang!")
}

func sayMessage(msg string) {
	fmt.Println(msg)
}
