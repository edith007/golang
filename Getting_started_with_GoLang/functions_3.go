package main

import (
	"fmt"
)

func main() {
	sayGreeting("Hello", "Siddharth")
}

func sayGreeting(greeting string, name string) {
	fmt.Println(greeting, name)
}
