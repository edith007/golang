package main

import (
	"fmt"
)

func main() {
	func() { // Anonymous function
		fmt.Println("Hello, GoLang!")
	}() // invoke function == defining and executing it at same time.
}
