package main

import (
	"fmt"
)

func main() {
	statePopulations := map[string]int{
		"Jaunpur":   1234567890,
		"Lucknow":   2345678901,
		"Delhi":     3456789012,
		"Guna":      4567891234,
		"Hyderabad": 5678901234,
	}
	if pop, ok := statePopulations["Lucknow"]; ok {
		fmt.Println(pop)
	}
}
