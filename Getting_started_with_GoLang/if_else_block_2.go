package main

import (
	"fmt"
)

func main() {
	number := 50
	guess := -5
	if guess < 1 {
		fmt.Println("The Guess must be between 1 and 100")
	} else if guess < 1 || guess > 100 {
		fmt.Println("The guess must be less than 100!")
	} else {
		if guess < number {
			fmt.Println("Too Low")
		}
		if guess > number {
			fmt.Println("Too High")
		}
		if guess == number {
			fmt.Println("You got it!")
		}
		fmt.Println(number <= guess, number >= guess, number != guess)
	}
}
