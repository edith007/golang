package main

import (
	"fmt"
)

type Animal struct {
	Name   string
	Origin string
}

type Bird struct {
	Animal
	SpeedKPH float32
	CanFLY   bool
}

func main() {
	b := Bird{
		Animal:   Animal{Name: "EMU", Origin: "Australia"},
		SpeedKPH: 48,
		CanFLY:   false,
	}
	fmt.Println(b)
}
