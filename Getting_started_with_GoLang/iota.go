package main

import (
	"fmt"
)

const (
	a = iota
	b = iota
	c = iota
)

func main() {
	fmt.Printf("%v, %v, %v\n", a, b, c)
}
