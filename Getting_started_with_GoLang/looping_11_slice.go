package main

import (
	"fmt"
)

func main() {
	statePopulations := map[string]int{
		"Jaunpur":   1234567890,
		"Lucknow":   2345678901,
		"Rishikesh": 3456789012,
		"Delhi":     4567890123,
		"Guna":      5678901234,
		"Hyderabad": 6789012345,
	}
	for k, v := range statePopulations {
		fmt.Println(k, v)
	}
}
