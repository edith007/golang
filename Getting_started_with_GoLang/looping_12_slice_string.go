package main

import (
	"fmt"
)

func main() {
	s := "Hello GoLang!"
	for k, v := range s {
		fmt.Println(k, v)
	}
}
