package main

import (
	"fmt"
)

func main() {
	i := 0 // i is scoped to the main function
	for ; i < 5; i++ {
		fmt.Println(i)
	}
	fmt.Println(i)
}
