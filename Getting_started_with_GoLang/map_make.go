package main

import (
	"fmt"
)

func main() {
	statePopulations := make(map[string]int)
	statePopulations = map[string]int{
		"california":   1234567890,
		"Texas":        2345678901,
		"New York":     3456789012,
		"Pennsylvania": 4567890123,
		"Illinois":     5678901234,
		"Ohio":         6789012345,
	}
	fmt.Println(statePopulations)
}
