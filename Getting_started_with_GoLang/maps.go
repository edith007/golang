package main

import (
	"fmt"
)

func main() {
	statePopulations := map[string]int{
		"California": 123456789,
		"Texas":      234567890,
		"Florida":    345678901,
		"New York":   456789012,
		"Illinois":   567890123,
		"Ohio":       678901234,
	}
	fmt.Println(statePopulations)
}
