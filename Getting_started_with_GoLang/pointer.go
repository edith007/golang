package main

import (
	"fmt"
)

func main() {
	a := 42
	b := a // copy the data of a and assign it to b
	fmt.Println(a, b)
	a = 27
	fmt.Println(a, b)
}
