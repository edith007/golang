package main

import (
	"fmt"
)

func main() {
	a := [3]int{1, 2, 3} // array which hold 3 values
	b := &a[0]           // b is pointing to the 1st element of a
	c := &a[1]           // c is pointing to the 2nd element of a
	fmt.Printf("%v %p %p\n", a, b, c)
}
