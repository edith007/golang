package main

import (
	"fmt"
)

type Doctor struct {
	Number     int
	ActorName  string
	Companions []string
	Episodes   []string
}

func main() {
	aDoctor := Doctor{
		Number:    3,
		ActorName: "Jon Pertwee",
		Companions: []string{
			"liz Shaw",
			"Jo Grant",
			"Sarah Jane Smith",
		},
	}
	fmt.Println(aDoctor)
}
