package main

import (
	"fmt"
)

func main() {
	switch 2 {
	case 1, 5, 10:
		fmt.Println("One")
	case 2, 4, 6:
		fmt.Println("Two")
	default:
		fmt.Println("Not one or two")
	}
}
