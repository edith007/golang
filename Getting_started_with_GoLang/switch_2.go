package main

import (
	"fmt"
)

func main() {
	i := 10
	switch {
	case i <= 10:
		fmt.Println("Less than or equal to 10")
	case i <= 20:
		fmt.Println("Less than or equal to twenty")
	default:
		fmt.Println("Greater than twenty")
	}
}
